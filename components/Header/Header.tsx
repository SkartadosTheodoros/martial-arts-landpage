import { useState } from 'react';
import NavBar from '../NavBar';
import SideBar from '../SideBar';

const Header = () => {
  const [isSideBarOpen, setSideBarOpen] = useState(false);

  const openSideBarHandler = () => setSideBarOpen(true);
  const closeSideBarHandler = () => setSideBarOpen(false);
  
  return (
    <>
      <NavBar onOpenSideBar={openSideBarHandler} />
      <SideBar isOpen={isSideBarOpen} onCloseSideBar={closeSideBarHandler} />
    </>
  );
};

export default Header;
