import React from 'react';
import Link from 'next/link';
import { FaBars } from 'react-icons/fa';
import { navLinks } from '../../utils/links';

import {
  NavWrapper,
  NavContainer,
  NavLogo,
  NavMenu,
  NavItem,
  NavLink,
  MobileIcon,
} from './styles';

type NavBarPropTypes = {
  onOpenSideBar: () => void;
};

const NavBar = ({ onOpenSideBar }: NavBarPropTypes) => {
  return (
    <header>
      <NavWrapper>
        <NavContainer>
          <Link href="/" passHref>
            <NavLogo>Martial Arts</NavLogo>
          </Link>

          <MobileIcon onClick={onOpenSideBar}>
            <FaBars />
          </MobileIcon>

          <NavMenu>
            {navLinks.map((link, index) => (
              <NavItem key={index}>
                <Link href={link.url} passHref>
                  <NavLink>{link.section}</NavLink>
                </Link>
              </NavItem>
            ))}
          </NavMenu>
        </NavContainer>
      </NavWrapper>
    </header>
  );
};

export default NavBar;
