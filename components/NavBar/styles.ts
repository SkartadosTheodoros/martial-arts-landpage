import styled from 'styled-components';

export const NavWrapper = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #000;
  font-size: 1rem;
  height: 80px;
  position: sticky;
  top: 0;
  z-index: 10;

  @media screen and (max-width: 960px) {
    transition: 1s all ease;
  }
`;

export const NavContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 24px;
  max-width: 1100px;
  width: 100%;
  height: 80px;
`;

export const NavLogo = styled.a`
  display: flex;
  justify-self: flex-start;
  align-items: center;
  margin-left: 24px;
  text-decoration: none;
  font-size: 1.5rem;
  font-weight: bold;
  color: #f00;
  cursor: pointer;
`;

export const NavMenu = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  text-align: center;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavItem = styled.li`
  height: 80px;
`;

export const NavLink = styled.a`
  display: flex;
  align-items: center;
  text-decoration: none;
  color: #fff;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;

  &:hover {
    border-bottom: 3px solid #f00;
  }
`;

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    font-size: 1.8rem;
    color: #fff;
    top: 0;
    right: 0;
    cursor: pointer;

    transform: translate(-100%, 60%);
  }
`;
