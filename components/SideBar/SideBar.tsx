import React from 'react';
import Link from 'next/link';
import { navLinks } from '../../utils/links';

import {
  SideBarWrapper,
  CloseIcon,
  IconContainer,
  SideBarContainer,
  SideBarMenu,
  SideBarItem,
  SideBarLink,
} from './styles';

type SideBarPropTypes = {
  isOpen: boolean;
  onCloseSideBar: () => void;
};

const SideBar = ({ isOpen, onCloseSideBar }: SideBarPropTypes) => {
  const linksNumber = navLinks.length;

  return (
    <SideBarWrapper isOpen={isOpen}>
      <IconContainer onClick={onCloseSideBar}>
        <CloseIcon />
      </IconContainer>

      <SideBarContainer>
        <SideBarMenu linksNumber={linksNumber}>
          {navLinks.map((link, index) => (
            <SideBarItem key={index}>
              <Link href={link.url} passHref>
                <SideBarLink onClick={onCloseSideBar}>
                  {link.section}
                </SideBarLink>
              </Link>
            </SideBarItem>
          ))}

          <SideBarLink></SideBarLink>
        </SideBarMenu>
      </SideBarContainer>
    </SideBarWrapper>
  );
};

export default SideBar;
