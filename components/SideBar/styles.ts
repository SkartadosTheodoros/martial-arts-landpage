import styled from 'styled-components';
import { FaTimes } from 'react-icons/fa';

export const SideBarWrapper = styled.aside`
  position: fixed;
  display: grid;
  align-items: center;
  background-color: #0d0d0d;
  opacity: ${({ isOpen }: { isOpen: boolean }) => (isOpen ? '100%' : '0')};
  top: ${({ isOpen }: { isOpen: boolean }) => (isOpen ? '0' : '-100%')};
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: ${({ isOpen }: { isOpen: boolean }) => (isOpen ? 100 : 1)};

  transition: 0.5s ease-in-out;
`;

export const CloseIcon = styled(FaTimes)`
  color: #fff;
`;

export const IconContainer = styled.div`
  position: absolute;
  background-color: transparent;
  font-size: 2rem;
  outline: none;
  top: 1.2rem;
  right: 1.6rem;
  cursor: pointer;
`;

export const SideBarContainer = styled.div`
  color: #fff;
`;

export const SideBarMenu = styled.ul`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: ${({ linksNumber }: { linksNumber: number }) =>
    `repeat(${linksNumber}, 80px)`};

  @media screen and (max-width: 480px) {
    grid-template-rows: ${({ linksNumber }: { linksNumber: number }) =>
      `repeat(${linksNumber}, 60px)`};
  }
`;

export const SideBarItem = styled.li`
  list-style: none;
`;

export const SideBarLink = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  font-size: 1.5rem;
  color: #fff;
  cursor: pointer;

  transition: 0.25s ease-in-out;

  &:hover {
    color: #f00;
    transition: 0.25s ease-in-out;
  }
`;
